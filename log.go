package log

import (
	"strings"
	"sync"

	"go.uber.org/zap"
)

var logger *zap.SugaredLogger
var s = sync.Once{}

func init() {
	s.Do(func() {
		l, _ := zap.NewProduction()
		logger = l.Sugar()
	})
}

type F map[string]interface{}

func Named(name string) {
	logger.Named(name)
}

func DevMode() {
	l, _ := zap.NewDevelopment()
	logger = l.Sugar()
}

func Debug(msg string, fields F) {
	Call(msg, fields, logger.Debugf, logger.Debugw)
}

func Info(msg string, fields F) {
	Call(msg, fields, logger.Infof, logger.Infow)
}

func Warn(msg string, fields F) {
	Call(msg, fields, logger.Warnf, logger.Warnw)
}

func Error(msg string, fields F) {
	Call(msg, fields, logger.Errorf, logger.Errorw)
}

func Panic(msg string, fields F) {
	Call(msg, fields, logger.Panicf, logger.Panicw)
}

func Fatal(msg string, fields F) {
	Call(msg, fields, logger.Fatalf, logger.Fatalw)
}

func Call(msg string, fields F, fnF, fnW func(string, ...interface{})) {
	defer logger.Sync()
	var f = make([]interface{}, 0)

	if strings.ContainsAny(msg, "%") {
		for _, v := range fields {
			f = append(f, v)
		}
		fnF(msg, f...)
		return
	}

	for k, v := range fields {
		f = append(f, k, v)
	}

	fnW(msg, f...)
}

